#!/bin/bash
# This file sets up the directory structure that is needed to run the docker-compose.yml file and then
# starts up all the services with docker compose

# First, read the environment variables from the .env file
source .env
echo "Starting setup of the data catalog repository structure in directory ${DATA_CATALOG_HOST_VOLUME}"

###########################################
# Timescale DB server directories and files
###########################################
# Now create the directory structure and install the configuration files for the Timescale DB server.
if [ ! -d ${DATA_CATALOG_HOST_VOLUME}/timescaledb ]; then
 echo "Creating ${DATA_CATALOG_HOST_VOLUME}/timescaledb"
 mkdir ${DATA_CATALOG_HOST_VOLUME}/timescaledb
else
 echo "${DATA_CATALOG_HOST_VOLUME}/timescaledb already exists"
fi

###########################################
# RabbitMQ server directories and files
###########################################
# Now create the directory structure and install the configuration files for the RabbitMQ server.
if [ ! -d ${DATA_CATALOG_HOST_VOLUME}/rabbitmq ]; then
 echo "Creating ${DATA_CATALOG_HOST_VOLUME}/rabbitmq"
 mkdir ${DATA_CATALOG_HOST_VOLUME}/rabbitmq
else
 echo "${DATA_CATALOG_HOST_VOLUME}/rabbitmq already exists"
fi
if [ ! -d ${DATA_CATALOG_HOST_VOLUME}/rabbitmq/mnesia ]; then
 echo "Creating ${DATA_CATALOG_HOST_VOLUME}/rabbitmq/mnesia"
 mkdir ${DATA_CATALOG_HOST_VOLUME}/rabbitmq/mnesia
else
 echo "${DATA_CATALOG_HOST_VOLUME}/rabbitmq/mnesia already exists"
fi

###########################################
# The Elasticsearch server persistence
###########################################
if [ ! -d ${DATA_CATALOG_HOST_VOLUME}/elasticsearch ]; then
    echo "Creating ${DATA_CATALOG_HOST_VOLUME}/elasticsearch"
    mkdir ${DATA_CATALOG_HOST_VOLUME}/elasticsearch
else
    echo "${DATA_CATALOG_HOST_VOLUME}/elasticsearch already exists"
fi

###########################################
# The Kibana Server
###########################################
if [ ! -d ${DATA_CATALOG_HOST_VOLUME}/kibana ]; then
    echo "Creating ${DATA_CATALOG_HOST_VOLUME}/kibana"
    mkdir ${DATA_CATALOG_HOST_VOLUME}/kibana
else
    echo "${DATA_CATALOG_HOST_VOLUME}/kibana already exists"
fi

###########################################
# The Geoserver
###########################################
if [ ! -d ${DATA_CATALOG_HOST_VOLUME}/geoserver ]; then
    echo "Creating ${DATA_CATALOG_HOST_VOLUME}/geoserver"
    mkdir ${DATA_CATALOG_HOST_VOLUME}/geoserver
else
    echo "${DATA_CATALOG_HOST_VOLUME}/geoserver already exists"
fi

# Check to see if there is a data directory for geoserver and if not, grab
# the default one off the web and expand it into the data directory
if [ ! -d ${DATA_CATALOG_HOST_VOLUME}/geoserver/data ]; then
    wget https://build.geo-solutions.it/geonode/geoserver/latest/data-2.14.x.zip -O ${DATA_CATALOG_HOST_VOLUME}/geoserver/data-2.14.x.zip --no-check-certificate
    unzip ${DATA_CATALOG_HOST_VOLUME}/geoserver/data-2.14.x.zip -d ${DATA_CATALOG_HOST_VOLUME}/geoserver

    # Now copy over the config file while replacing the base URL so the geoserver URLs are correct
    sed "s/DATA_CATALOG_HOST_NAME/${DATA_CATALOG_HOST_NAME}/" components/geoserver/global.xml > ${DATA_CATALOG_HOST_VOLUME}/geoserver/data/global.xml
else
    echo "${DATA_CATALOG_HOST_VOLUME}/geoserver/data directory already exists"
fi
