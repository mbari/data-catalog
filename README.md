# Data Catalog Services

This repository basically contains a Docker compose file and some configuration files to allow you to create a software stack that will serve as a catalog surrounding folders on a file store.  It really is meant more for development and testing as its brings up RabbitMQ, Elasticsearch, Kibana, etc., etc. and while that's fine for development and test, in production, you would split these components out.  This docker-compose file let's you monitor multiple directories (each is called a 'repo') on the host system.  Because there are individual docker images for the various components, you will have to edit the docker-compose file to get it configured for each directory you want to monitor.

## Installation

1. Decide which directories you would like to monitor and index on your host system.  For this example, we will use:
    1. /path/to/my/repo-1
    1. /path/to/my/repo-2
1. Decide what you want to call each of the repositories associated with each directory.  For this examples, we will call them 'repo-1' and 'repo-2'. Note they should be short names with no spaces and all lower case.
1. You must have Docker (and Docker compose) installed on your host.
1. You must not be running a web server as this stack of software will run an HTTP server (over port 443) that will be a proxy for all other services.
1. Make a copy of the .env_template file, name it .env and define the environment variables that are listed in the file.
1. Next, copy ./components/timescaledb/init-repo-db.sh.template to ./components/timescaledb/init-repo-db.sh.  Then, you will want to edit the ./components/timescaledb/init-repo-db.sh file.  There are directions in the file, but basically you want to copy and paste a block of 5 lines in the script and replace the 'repo-name' with the name of the repository the database will be for.  There should be a block of those 5 lines repeated for each for the 'repo' names you are creating.
1. Now copy docker-compose.yml.template to docker-compose.yml.  Next you will need to edit the docker-compose.yml file and there are two services that are templates that you will need to edit to point to your repository:
    1. In the file-to-es-indexer-repo-1 entry, you need to change the following:
        1. FTESI_REPO_NAME= should be the 'name' of the repository this service will monitor.  Replace 'repo-1' with whatever name you chose for your repo.
        1. FTESI_DATA_REPO_BASE_URL= should point to the 'name' of the repo you chose so change the very last 'repo-1' and replace it with the name you chose for your repo.
        1. FTESI_RABBITMQ_EXCHANGE_NAME= should also be the 'name' of your repo so change the 'repo-1' to the name you chose for your repo.
        1. Under the 'volumes' section, replace '/path/to/my/repo-1' with the absolute path to the repo on your host that will be monitored.
    1. In the fsmon-repo-1 section, you need to change the following:
        1. FSMON_BASE_URL= replace the last 'repo-1' with the name you chose for your repo.
        1. FSMON_REPO_NAME= replace 'repo-1' with the name you chose for your repo.
        1. Under the 'volumes' section, replace '/path/to/my/repo-1' with the absolute path to the repo on your host that will be monitored.
    1. In the httpd section, you need to mount your repo to the correct location, change the first line under 'volumes' and change '/path/to/my/repo-1' to the absolute path to your repo and change the last 'repo-1' to the name you chose for the repo.
1. If you are monitoring more than one repo with this stack, you will need to do the following in the docker-compose.yml file:
    1. Copy the file-to-es-indexer-repo-1 and fsmon-repo-1 sections and paste them into the docker-compose.yml (putting them below the fsmon-repo-1 section is a good idea).  You need to edit the properties in the pasted section just as you did in the previous step, but using the second repo name you chose.  In addition you will need to:
        1. Change the service name from file-to-es-indexer-repo-1 to file-to-es-indexer-repo-2 and change fsmon-repo-1 to fsmon-repo-2.
        1. Change the 'hostname' under each section so that do not clash with the first repo settings.  For example, you could change file-to-es-indexer-repo-1 to file-to-es-indexer-repo-2 and fsmon-repo-1 to fsmon-repo-2.
        1. Copy and past the first line under the 'volumes' section under 'httpd' and change it to match the path and name to your second repo.
    1. You can repeat the previous steps for each additional repo you want to monitor on this host.
1. You will also want to edit the components/httpd/index.html file and edit the first line and replace 'repo-1' with the name of your first repo.  You can then copy, paste and edit that line for each repository being monitored.
1. Next, you need to create (or get) an SSL certificate. In order to provide SSL for the repository server an SSL certificate and key must be generated (or obtained) before docker is running. Normally for production, you would get this certificate from a CA, but for development, you can run the following to generate them. NOTE: Your entry for the 'Common Name' should match the DATA_CATALOG_HOST_NAME that is defined in the .env file. This will generate a certificate/key pair and will store it in the directory where you ran this command.

```bash
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ./components/httpd/server.key -out ./components/httpd/server.crt
```

1. If running locally and you want a name for the server, update /etc/hosts file to point your DATA_CATALOG_HOST_NAME to 127.0.0.1
1. chmod a+x run.sh
1. Run run.sh
1. And lastly, run 'docker-compose up'

## TODOs

1. Document how to get Slack connection setup.
1. Figure out how to add repositories after initial startup.  Adding to the index.html and docker-compose.yml files is straightforward but you have to create a new timescaledb as this is only initialized when the server is started for the first time.  Can run each step manually through psql or something like Aqua Data Studio, but not very scalable.
1. Try to automate more of this so you don't have to edit so many files in so many places.
